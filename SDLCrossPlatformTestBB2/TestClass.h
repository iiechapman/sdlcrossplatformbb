//
//  TestClass.h
//  SDLCrossPlatformTestBB2
//
//  Created by Evan Chapman on 12/22/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#ifndef __SDLCrossPlatformTestBB2__TestClass__
#define __SDLCrossPlatformTestBB2__TestClass__

#include <iostream>

class Test{
public:
    void Speak();
};

#endif /* defined(__SDLCrossPlatformTestBB2__TestClass__) */
