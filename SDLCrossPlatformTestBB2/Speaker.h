//
//  Speaker.h
//  SDLCrossPlatformTestBB2
//
//  Created by Evan Chapman on 12/22/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#ifndef __SDLCrossPlatformTestBB2__Speaker__
#define __SDLCrossPlatformTestBB2__Speaker__

#include <iostream>
#include "TestClass.h"
class Speaker
{
public:
    Speaker();
    void Speak();
private:
    Test speaker;
};

#endif /* defined(__SDLCrossPlatformTestBB2__Speaker__) */
